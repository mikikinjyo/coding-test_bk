// test
class Message {
  constructor (message) {
    this._message = message;
  }
  helloWorld () {
    console.log(this._message);
  }
}

export default Message;
